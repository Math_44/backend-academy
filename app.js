const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require ('cors');

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

const rotaUsers = require ('./routes/cadastro');
const rotaAdmUser = require ('./routes/usuarios');


app.use("/users", rotaUsers);
app.use("/adm", rotaAdmUser);

app.use((req, res, next) => {

    const erro = new Error('Não encontrado');
    erro.status = 404;
    next(erro);


});


app.use((error, req, res, next) => {

    res.status(error.status || 500);
    return res.send({

        erro: {

            mensagem: error.message
        }


    });

});



module.exports = app;