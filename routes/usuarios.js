const express = require ('express');
const router = express ('router');


const usuario = require ('../controllers/usuario-controller');
const login = require('../middleware/login');

router.post('/cadastro',login, usuario.PostUsuario);
router.get("/", usuario.get);
router.get('/:id', usuario.getId);
router.post('/login',usuario.Login);

module.exports= router;