const express = require ('express');
const router = express.Router();

const cadastro = require ('../controllers/cadastro-controller');
const login = require('../middleware/login');


router.get('/',login, cadastro.GetUsers);
router.get('/pesquisa', cadastro.pesquisaUsers);
router.post('/', cadastro.postUsers);
router.patch('/:id_usuario',login, cadastro.pathUsers);
router.get('/:id_usuario',login, cadastro.GetUsersId);
router.delete('/:id_usuario', cadastro.DeleteUsers);
module.exports= router;
