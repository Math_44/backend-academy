const bcrypt = require('bcrypt');
const mysql = require('../mysql').pool;
const jwt = require('jsonwebtoken');


exports.get= (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }

        conn.query('SELECT * FROM admusuario',

            [req.params.id],

            (error, result, fields) => {

                if (error) { return res.status(500).send({ error: error }) };


                const response ={ 

                    Usuarios: result.map(prod =>{

                        return {

                            id: prod.id,
                            nome: prod.nome,
                            email: prod.email


                        }


                    })

                }

                return res.status(201).send(response);
            }

        )

    })

}



exports.getId = (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }

        conn.query('SELECT * FROM admusuario WHERE id = ? ;',

            [req.params.id],

            (error, result, fields) => {

                if (error) { return res.status(500).send({ error: error }) };


                if (result.length == 0) {

                    return res.status(404).send({

                        mensagem: 'Não foi encontrado nenhum Usuario com este ID'

                    });

                };

                const Usuario ={


                    

                        id: result[0].id,
                        nome: result[0].nome,
                        email: result[0].email

                    


                }

                return res.status(201).send(Usuario);
            }

        )

    })

}


exports.PostUsuario = (req, res, next) => {

    mysql.getConnection((error, conn) => {

        if (error) { return res.status(500).send({ error }) }

        conn.query('SELECT * FROM admusuario WHERE email = ? ;', [req.body.email], (error, result) => {

            if (error) { return res.status(500).send({ error }) }
            if (result.length > 0) {

                res.status(404).send({ mensagem: 'Adm ja cadastrado' });

            } else {

                bcrypt.hash(req.body.senha, 10, (errBcrypt, hash) => {

                    if (errBcrypt) { return res.status(500).send({ error: errBcrypt }) }

                    conn.query(

                        'INSERT INTO admusuario (nome, email , senha) VALUES (? ,?,?);',


                        [

                            req.body.nome,
                            req.body.email,
                            hash

                        ],

                        (error, result, fields) => {

                            conn.release();

                            if (error) { return res.status(500).send({ error }) }

                            const response = {

                                mensagem: "Administrador cadastrado",

                                Usuario: {

                                    id: result.id,
                                    nome: req.body.nome,
                                    email: req.body.email


                                }



                            }

                            return res.status(201).send(response);


                        })

                });


            }


        })


    });

}



exports.Login = (req, res, next) => {

    mysql.getConnection((error, conn) => {

        if (error) { return res.status(500).send({ error }) };
        const query = `SELECT * FROM admusuario WHERE email =? ;`;
        conn.query(query, [req.body.email], (error, results, fields) => {
            conn.release();
            if (error) { return res.status(500).send({ error }) };
            if (results.length < 1) { return res.status(401).send({ mensagem: 'Falha na autenticação' }) };
            bcrypt.compare(req.body.senha, results[0].senha, (error, result) => {

                if (error) {

                    return res.status(401).send({ mensagem: 'Falha na autenticação' });

                }

                if (result) {
                    const token = jwt.sign({
                        id_usuario: results[0].id,
                        nome: results[0].nome,
                        email: results[0].email

                    }, process.env.JWT_KEY, {

                        expiresIn: "1h"

                    });
                    return res.status(200).send({
                        mensagem: 'Autenticado com sucesso',
                        token: token,
                        Usuario:{
                            id: results[0].id,
                            nome: results[0].nome,
                            email: results[0].email

                        }

                    });

                }

                return res.status(401).send({ mensagem: 'Senha ou email incorreto' });


            });

        })


    })



}