const { request, response } = require('express');

const mysql = require('../mysql').pool;

exports.GetUsers = (req, res, next) => {

    mysql.getConnection((error, conn) => {

        if (error) { return res.status(500).send({ error }) };

        conn.query(

            `SELECT *, DATE_FORMAT(data_nascimento,'%d/%m/%Y') AS niceDate 
           FROM cadastro
           ORDER BY data_nascimento DESC 
           LIMIT 0,14; `,

            (error, result, fields) => {

                if (error) { return res.status(500).send({ error }) };

                const response = {

                    // quantidade: result.length,

                    Usuarios: result.map(prod => {

                        return {

                            id_usuario: prod.id_usuario,
                            nome: prod.nome,
                            cpf: prod.cpf,
                            data_nascimento: prod.niceDate


                        }

                    })


                }

                return res.status(201).send(response);
            }

        );

    })

};


exports.postUsers = (req, res, next) => {

    mysql.getConnection((error, conn) => {
        
        if (error) { return res.status(500).send({ error }) };

        conn.query(

            'SELECT * FROM cadastro WHERE cpf = ?;',
            
            [req.body.cpf],

            (error, result, fields) => {

                if (error) { return res.status(500).send({ error }) };

                if (result.length > 0) {

                    return res.status(401).send({ mensagem: 'Cpf ja existe' })

                } else {


                    conn.query(

                        'INSERT INTO cadastro (nome, cpf , data_nascimento ) VALUES (? , ? , ? );',

                        [

                            req.body.nome,
                            req.body.cpf,
                            req.body.data_nascimento

                        ],

                        (error, result, fields) => {

                            

                            conn.release();

                            if(result == null){

                                return res.status(404).send({ mensagem : 'data invalida' })

                            }

                            if (error) { return res.status(500).send({ error }) };


                            const response = {

                                mensagem: 'Cadastro conluido',

                                Usuario: {

                                    id_usuario: result.id_usuario,
                                    nome: req.body.nome,
                                    cpf: req.body.cpf,
                                    data_nascimento: req.body.data_nascimento

                                }

                            }

                            return res.status(201).send(response);

                        }

                    );


                }

            }

        )

    })

};


exports.pathUsers = (req, res, next) => {

    mysql.getConnection((error, conn) => {

        if (error) { return res.status(500).send({ error }) };

        conn.query(

            ` UPDATE cadastro
             SET nome           = ?,
             cpf                = ?,
             data_nascimento    = ?
             WHERE id_usuario   = ? ; `,

            [

                req.body.nome,
                req.body.cpf,
                req.body.data_nascimento,
                req.body.id_usuario

            ],

            (error, result, fields) => {

                if (error) { return res.status(500).send({ error: error }) };


                const response = {


                    Usuario: {

                        id_usuario: req.body.id_usuario,
                        nome: req.body.nome,
                        cpf: req.body.cpf,
                        data_nascimento: req.body.data_nascimento

                    }


                }

                return res.status(202).send(response);
            }

        );

    })

};




exports.GetUsersId = (req, res, next) => {

    mysql.getConnection((error, conn) => {

        if (error) { return res.status(500).send({ error }) };

        conn.query(

            `SELECT *,DATE_FORMAT(data_nascimento,'%Y/%m/%d') AS data_nascimento
            FROM cadastro WHERE id_usuario= ? ORDER BY data_nascimento DESC 
            LIMIT 0,14;`,

            // 'SELECT * FROM cadastro WHERE id_usuario = ? ;',

            [req.params.id_usuario],
            (error, result, fields) => {

                if (error) { return res.status(500).send({ error }) };


                if (result.length == 0) {

                    return res.status(404).send({

                        mensagem: 'Não foi encontrado nenhum Usuario com este ID'

                    });

                };


                const response = {


                    id_usuario: result[0].id_usuario,
                    nome: result[0].nome,
                    cpf: result[0].cpf,
                    data_nascimento: result[0].data_nascimento


                }

                return res.status(201).send(response);
            }

        );

    })

};


exports.DeleteUsers = (req, res, next) => {

    mysql.getConnection((error, conn) => {

        if (error) { return res.status(500).send({ error }) }

        conn.query(

            'DELETE FROM cadastro WHERE id_usuario = ? ;',

            [req.params.id_usuario],

            (error, result, fields) => {

                if (error) { return res.status(500).send({ error }) }


                conn.release();

                const response = {

                    mensagem: 'Usuario excluido'


                }
                return res.status(202).send(response);

            }


        )

    })

};


exports.pesquisaUsers = (req, res, next) => {

    mysql.getConnection((error, conn) => {

        if (error) { return res.status(500).send({ error }) };

        conn.query(

            `SELECT *,DATE_FORMAT(data_nascimento,'%d/%m/%Y') AS niceDate 
            FROM cadastro WHERE nome = ? ORDER BY data_nascimento DESC 
            LIMIT 0,14;`, [req.body.nome], (error, result, fields) => {

            if (error) { return res.status(500).send({ error }) }


            if (result.length == 0) {

                conn.query(

                    `SELECT *,DATE_FORMAT(data_nascimento,'%d/%m/%Y') AS niceDate
                     FROM cadastro WHERE cpf = ? ORDER BY data_nascimento DESC
                     LIMIT 0,14;`, [req.body.cpf], (error, result, fields) => {

                    if (error) { return res.status(500).send({ error }) }

                    const response = {

                        Usuario: {

                            nome: result[0].nome,
                            cpf: result[0].cpf,
                            data_nascimento: result[0].niceDate

                        }

                    }

                    return res.status(201).send(response);

                }

                )

            } else {

                const response = {

                    Usuario: {

                        nome: result[0].nome,
                        cpf: result[0].cpf,
                        data_nascimento: result[0].niceDate

                    }

                }
                return res.status(201).send(response);

            }


        }

        );


    });


}
